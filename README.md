![drachtio logo](/config/drachtio.png "MarineGEO logo")

# **DRACHTIO SIP PROXY SIMPLE**

*This is basic drachtio application that handles registration and proxies SIP INVITEs.*


![drachtio logo](/config/FLOWCHART.png "MarineGEO logo")

## **Features**

The drachtio sip proxy simple main focus on:

* Handles sip registration from sip client

* Proxies SIP INVITEs to PBX via Drachtio Server

## **Terminologies**

* Middleware: A middleware represents a set of services, typically functions, that are organized in a pipeline and are responsible for processing incoming requests and relative responses.

* Sip Client: SIP client is a program that you install on your computer or mobile device. ... You can use them with any SIP account, and can even use them within a PBX environment.

* PBX: A PBX is an acronym for Private Branch Exchange, which is a private telephone network that allows users can talk to each other


## **Paradigm**


* Support REGISTER, INVITE, OPTIONS packet

## **Components**

* Middlewares
* Sip Client
* PBX
* Drachtio Server

## **Installation**

* Edit the configuration file(config folder) as needed, then:

> `$npm install`

> `$node app.js`

## **Draft**

`https://docs.google.com/presentation/d/1FvfuxOA6bnXGh2qGqtTLmjrXKxQERFn2pJ6eqrQ89-A/edit?usp=sharing`

## **Configuration**

The configuration file is config/default.json. It is pretty-self explanatory. It includes:

* Log level setting

* Location of drachtio server to connect to

* A simple database of user / passwords organized by sip domain.

## **Author**

**Dave Horston**







