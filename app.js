/**
 * Require() using for include external modules that exist in separate files. 
 * require() statement basically reads a JavaScript file, executes it, 
 * and then proceeds to return the export object.
 */

//---------------------+---------------------+---------------
const Srf = require('drachtio-srf');
const srf = new Srf();   // init instance of a drachtio server
//---------------------+---------------------+---------------

//---------------------+---------------------+---------------
const config = require('config');  
/** 
* Node-config organizes hierarchical configurations for your app deployments.(https://www.npmjs.com/package/config)
* config.get() will throw an exception for undefined keys to help catch typos and missing values. 
**/
//---------------------+---------------------+---------------

//---------------------+---------------------+---------------
const logger = require('pino')(config.get('logging')); 
/**
 (https://www.npmjs.com/package/pino#low-overhead)
 Pino is super fast, all natural json logger
 */
//---------------------+---------------------+---------------

//---------------------+---------------------+---------------
const regParser = require('drachtio-mw-registration-parser') ;
const digestChallenge = require('./lib/middleware');
//Init instance of middleware using for authentication
//---------------------+---------------------+---------------

//---------------------+---------------------+---------------
srf.connect(config.get('drachtio'));
srf.on('connect', (err, hp) => {
  if (err) throw err;
  logger.info(`connected to drachtio listening on ${hp}`);
})
  .on('error', (err) => logger.error(err));
/** 
 * Get value configuration of Drachtio from JSON object,We listened for the 'connect' event on the srf object
 * The type of connection made in our example above is called an inbound connection; that is, a TCP connection made from the nodejs application acting as a client to the drachtio server process acting as the server
 * Pro tip: always have an error handler on your Srf instance when using inbound connections, so your application will automatically reconnect to the server if the tcp connection is dropped.
 */

//---------------------+---------------------+---------------

srf.use('register', [regParser, digestChallenge]); //Install middleware using use(method,handler) method to handle and authentication incoming request
srf.invite(require('./lib/invite')({logger})); // Tell drachtio server it's will be invoke function to handle INVITE request
srf.register(require('./lib/register')({logger})); // Tell drachtio server it's will be invoke function to handle REGISTER request

//---------------------+---------------------+---------------
