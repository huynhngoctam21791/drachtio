/**
 * Require() using for include external modules that exist in separate files. 
 * require() statement basically reads a JavaScript file, executes it, 
 * and then proceeds to return the export object.
 */
//---------------------+---------------------+---------------
const auth = require('drachtio-mw-digest-auth') ; //Performs SIP Digest-based authentication for a user agent server (UAS) or proxy built using drachtio-srf
const config = require('config');
const parseUri = require('drachtio-srf').parseUri; //Static property returning a function that parses a SIP uri into Object
const domains = config.get('domains'); //get (value) of domains-object(key) in JSON file and return Object in domains constant
//---------------------+---------------------+---------------

//---------------------+---------------------+---------------
// Function return TRUE or FALSE (boolean value) 
// Purpose: check value of domains if it exists or not based on JSON object
function isValidDomain(domain) {
  console.log(`checking domain: ${domain} in ${JSON.stringify(domains)}`);
  return domain in domains; 
}
//---------------------+---------------------+---------------

//---------------------+---------------------+---------------
//  This is boolean function return STRING type of password of User in database JSON object if TRUE, else will return FALSE
function getUserPassword(domain, username) {
  const users = domains[domain];
  console.log(`checking for ${username} in ${JSON.stringify(users)}`);
  if (username in users) return users[username];
}
//---------------------+---------------------+---------------


//---------------------+---------------------+---------------
/**
 *  Providing an object that (optionally) specifies the sip realm to use in challenges,
 *  and a function that provides (via callback) the password for a given username and sip realm
 * Format
 * auth({
        key: value function(){    //realm: '127.0.0.1'

       },
        key: function(){   //

       }
    })
 */
module.exports = auth({
  realm: (req) => {
    const uri = parseUri(req.uri);
    // For example rURI: INVITE sip:5083084809@127.0.0.1:5060;tgrp=NYC-2 SIP/2.0
    // console.log ==>
    // {
    //   "family": "ipv4",
    //   "schema": "sip",
    //   "user": "5083084809",
    //   "host": "127.0.0.1",
    //   "port": 5060,
    //    "params": {
    //      "tgrp": "NYC-2"
    //    },
    //    "headers": {}
    // }
    //
    return isValidDomain(uri.host) ? uri.host : null;
  },
  passwordLookup: (username, realm, callback) => {
    const password = getUserPassword(realm, username); //return boolean value
    if (password) return callback(null, password); // if true, set password for sip
    return callback(new Error(`unknown user ${username}`));
  }
});
//---------------------+---------------------+---------------
